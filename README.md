![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

---

# Personal Website

This website will contain:

- Projects: This will contain open source project that I have worked on + personal projects.
- CV: This is my resume (maybe a link to stackoverflow).


# Development

To start developing simply run the following commands

```
npm install
npm run dev
```

# Build the public pages

By default the pages you are developing will not be public. You will need to run a command for this.

```
npm run build
```

This will recompile everything that is needed. The files will now be minified as well.
